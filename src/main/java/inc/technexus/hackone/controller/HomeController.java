package inc.technexus.hackone.controller;

import inc.technexus.hackone.model.AdviceData;
import inc.technexus.hackone.service.WeatherService;
import net.aksingh.owmjapis.api.APIException;
import net.aksingh.owmjapis.model.CurrentUVIndex;
import net.aksingh.owmjapis.model.CurrentWeather;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class HomeController {

    @Autowired
    private WeatherService weatherService;

    @GetMapping(value = "/")
    public String index(Model model) throws APIException {
       return "index";
    }

//    @GetMapping("/weather")
//    public String weather(@RequestParam(required = true, name = "zipcode") int zipCode, Model model) throws APIException {
//        model.addAttribute("zipcode", zipCode);
//        CurrentWeather currentWeather = weatherService.getWeather(zipCode);
//        model.addAttribute("currentWeather", currentWeather);
//        CurrentUVIndex uvIndex = weatherService.getUVIndex(47.6062,122.3321);
//        model.addAttribute("currentUVIndex", uvIndex);
//        return "advice";
//    }
//
//    @GetMapping("/uvindex")
//    public String uv(@RequestParam(required = true, name = "lat") double latitude, @RequestParam(required = true, name = "lng") double longitude, Model model) throws APIException {
//        CurrentUVIndex uvIndex = weatherService.getUVIndex(latitude,longitude);
//        model.addAttribute("currentUVIndex", uvIndex);
//        return "advice";
//    }

    @GetMapping("/advice")
    public String uv(@RequestParam(required = true, name = "zipcode") int zipCode, Model model) throws APIException {
        AdviceData adviceData = weatherService.advice(zipCode);
        model.addAttribute("adviceData", adviceData);
        model.addAttribute("zipCode", zipCode);
        return "advice";
    }

}

