package inc.technexus.hackone.model;

public class FireData {

    private String message;
    private String icon;
    private String time;
    private String fireSize;
    private String activity;

    //majorpollutant

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    
    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String t){
        time=t;
    }

    public void setFireSize(String s){
        fireSize = s;
    }

    public String getFireSize(String s){
        return fireSize;
    }

    public void setFireActivity(String a){
        activity = a;
    }

    public String getActivity(String s){
        return activity;
    }


}
