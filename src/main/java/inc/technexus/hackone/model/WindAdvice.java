package inc.technexus.hackone.model;

public class WindAdvice {
    private String message;

    private double speed;

    private String icon;

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double windspeed) {
        speed = windspeed;
    }
}
