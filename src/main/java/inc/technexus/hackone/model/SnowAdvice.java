package inc.technexus.hackone.model;

import java.util.Date;

public class SnowAdvice {

    private String message;

    public boolean hasSnow;

    private String icon;

    private String advertisment;



    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean getHasSnow(){
        return hasSnow;
    }

    public void setHasSnow(boolean hasSnow) {
        this.hasSnow = hasSnow;
    }

    public void setAdvertisment(String a){advertisment=a;}

    public String getAdvertisment(){return advertisment;}

}
