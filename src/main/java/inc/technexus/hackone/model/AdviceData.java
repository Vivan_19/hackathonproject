package inc.technexus.hackone.model;

import inc.technexus.hackone.service.WeatherService;
import net.aksingh.owmjapis.core.OWM;
import net.aksingh.owmjapis.model.CurrentWeather;

public class AdviceData {
    private SunAdvice sunAdvice;
    private HumidityAdvice humidityAdvice;
    private RainAdvice rainAdvice;
    private UVAdvice uvAdvice;
    private WindAdvice windAdvice;
    private AirQualityData airQualityData;
    private SnowAdvice snowAdvice;
    private PollenAdvice pollenAdvice;
    private FireAdvice fireAdvice;

    public AirQualityData getAirQualityData() {
        return airQualityData;
    }

    public void setAirQualityData(AirQualityData airQualityData) {
        this.airQualityData = airQualityData;
    }

    private String city;

    public String getCity(){
        return city;
    }

    public void setCity(String c){
        city = c;
    }

    public SunAdvice getSunAdvice() {
        return sunAdvice;
    }

    public void setSunAdvice(SunAdvice sunAdvice) {
        this.sunAdvice = sunAdvice;
    }

    public HumidityAdvice getHumidityAdvice() {
        return humidityAdvice;
    }

    public void setHumidityAdvice(HumidityAdvice humidityAdvice) {
        this.humidityAdvice = humidityAdvice;
    }

    public RainAdvice getRainAdvice() {
        return rainAdvice;
    }

    public void setRainAdvice(RainAdvice rainAdvice) {
        this.rainAdvice = rainAdvice;
    }

    public UVAdvice getUvAdvice() {
        return uvAdvice;
    }

    public void setUvAdvice(UVAdvice uvAdvice) {
        this.uvAdvice = uvAdvice;
    }

    public WindAdvice getWindAdvice() {
        return windAdvice;
    }

    public void setWindAdvice(WindAdvice windAdvice) {
        this.windAdvice = windAdvice;
    }

    public SnowAdvice getSnowAdvice() {
        return snowAdvice;
    }

    public void setSnowAdvice(SnowAdvice snowAdvice) {
        this.snowAdvice = snowAdvice;
    }

    public PollenAdvice getPollenAdvice(){ return pollenAdvice; }

    public void setPollenAdvice(PollenAdvice pollenAdvice) {this.pollenAdvice = pollenAdvice;}

    public FireAdvice getFireAdvice(){return fireAdvice;}

    public void setFireAdvice(FireAdvice fireAdvice){this.fireAdvice = fireAdvice;}
}
