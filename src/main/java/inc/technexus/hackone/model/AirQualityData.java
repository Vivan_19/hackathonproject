package inc.technexus.hackone.model;

public class AirQualityData {

    private String message;
    private int aqi;
    private String icon;
    private String time;
    //majorpollutant

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getAqi() {
        return aqi;
    }

    public void setAqi(int aqi) {
        this.aqi = aqi;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getTime() {
        return time;
    }
    public void setTime(String t){
        time=t;
    }


}
