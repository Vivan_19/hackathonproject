package inc.technexus.hackone.model;

public class UVAdvice {
    private String message;

    private double UVAIndexValue;

    private String icon;

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public double getUVAIndexValue() {
        return UVAIndexValue;
    }

    public void setUVAIndexValue(double value) {
        UVAIndexValue = value;
    }
}
