package inc.technexus.hackone.model;

public class PollenAdvice {
    private String grassMessage;
    private String treeMessage;
    private String weedMessage;
    private boolean pollenGrass;
    private boolean pollenTree;
    private boolean pollenWeed;
    private String advertisment;
    private String icon;
    private String time;

    public String getGrassMessage() { return grassMessage; }

    public void setGrassMessage(String message) { this.grassMessage = message; }

    public String getTreeMessage() {
        return treeMessage;
    }

    public void setTreeMessage(String message) {
        this.treeMessage = message;
    }

    public String getWeedMessage() {
        return weedMessage;
    }

    public void setWeedMessage(String message) {
        this.weedMessage = message;
    }

    public boolean getPollenGrass() {
        return pollenGrass;
    }

    public void setPollenGrass(boolean polleng) {this.pollenGrass = polleng;}

    public boolean getPollenTree() {return pollenTree;}

    public void setPollenTree(boolean pollent) {this.pollenTree = pollent;}

    public boolean getPollenWeed(){return pollenWeed;}

    public void setPollenWeed(boolean pollenw){this.pollenWeed = pollenw;}

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getTime() { return time; }

    public void setTime(String t){
        time=t;
    }

    public void setAdvertisment(String a){advertisment=a;}

    public String getAdvertisment(){return advertisment;}
}
