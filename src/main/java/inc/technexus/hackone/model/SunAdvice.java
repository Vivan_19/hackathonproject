package inc.technexus.hackone.model;

import java.util.Date;

public class SunAdvice {

    private String message;

    private int temperatureF;

    private int temperatureC;

    private String icon;

    private Date sunrise;

    private Date sunset;



    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getTemperatureF() {
        return temperatureF;
    }

    public void setTemperatureF(int temperature) {
        this.temperatureF = temperature;
    }

    public int getTemperatureC() {
        return temperatureC;
    }

    public void setTemperatureC(int temperature) {
        this.temperatureC = temperature;
    }

    public Date getSunrise()
    {
        return sunrise;
    }

    public Date getSunset() {
        return sunset;
    }
    public void setSunset(Date sunset) {
        this.sunset = sunset;
    }

    public void setSunrise(Date sunrise) {
        this.sunrise = sunrise;
    }
}
