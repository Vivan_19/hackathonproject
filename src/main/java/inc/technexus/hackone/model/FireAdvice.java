package inc.technexus.hackone.model;

public class FireAdvice {
    private String message;
    private String activity;
    private String time;
    private String icon;
    private String fireSize;


    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public String getTime() {
        return time;
    }

    public void setTime(String t) {
        this.time = t;
    }

    public String getActivity(){return activity;}

    public void setActivity(String a) {activity = a;}

    public String getFireSize(){return fireSize;}

    public void setFireSize(String s){fireSize = s;}


}

