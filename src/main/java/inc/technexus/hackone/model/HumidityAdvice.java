package inc.technexus.hackone.model;

public class HumidityAdvice {
    private String message;

    private double humidityPercent;

    private String icon;

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public double getHumidityPercent() {
        return humidityPercent;
    }

    public void setHumidityPercent(double humidity) {
        humidityPercent = humidity;
    }
}
