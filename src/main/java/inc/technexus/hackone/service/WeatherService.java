package inc.technexus.hackone.service;

import com.fasterxml.jackson.databind.JsonNode;
import inc.technexus.hackone.model.*;
import net.aksingh.owmjapis.api.APIException;
import net.aksingh.owmjapis.core.OWM;
import net.aksingh.owmjapis.model.CurrentUVIndex;
import net.aksingh.owmjapis.model.CurrentWeather;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import sun.tools.asm.CatchData;

import java.util.Date;

@Service
public class WeatherService {

    private static final String API_KEY = "26032910ae5883d482790827b048edfd";

    private static final String ZIPCODE_API_KEY="I3rB8lh4RnJkUSKTETWYennkvvh20MFnzMoYOAFdIJLz9G6ETu1tUjnZF88x2WBk";

    private static final String BREEZO_METER_API_KEY ="d0674cba20eb4fd1be0dbc1b70c26720";

    private String zipCodeUrl = "https://www.zipcodeapi.com/rest/" + ZIPCODE_API_KEY+ "/multi-info.json/{zipcode}/degrees";

    public CurrentWeather getWeather(int zipCode) throws APIException {
        OWM owm = new OWM(API_KEY);
        CurrentWeather currentWeather = owm.currentWeatherByZipCode(zipCode);
        System.out.println("City: " + currentWeather.getCityName());
        System.out.println("Temperature: " + (1.8 * (currentWeather.getMainData().getTemp() - 273) + 32)); //farenheit
        if (currentWeather.hasRainData()==true){
            System.out.println("Rain Forecast: " + currentWeather.getRainData());
        }
        System.out.println("Cloudiness: " + currentWeather.getCloudData().getCloud());
        if (currentWeather.getWindData().hasSpeed() == true) {
            System.out.println("Wind Speed: " + currentWeather.getWindData().getSpeed());
        }
        System.out.println("Humidity: " + currentWeather.getMainData().getHumidity());
        System.out.println("Sunrise: " + currentWeather.getSystemData().getSunriseDateTime());
        System.out.println("Sunset: " + currentWeather.getSystemData().getSunsetDateTime());
        if (currentWeather.hasSnowData()==true) {
            System.out.println("Snow: " + currentWeather.getSnowData());
        }
        return currentWeather;
    }

    public CurrentUVIndex getUVIndex(double lat, double lng) throws APIException {
        OWM owm = new OWM((API_KEY));
        CurrentUVIndex currentUVIndex = owm.currentUVIndexByCoords(lat, lng);
        System.out.println("UV Index: " + currentUVIndex.getValue());
        return currentUVIndex;
    }

    public AirQualityData getAirQualityDataFromZip(String lat, String lng){
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        HttpEntity httpEntity = new HttpEntity(headers);
        String aqiApi = "https://api.breezometer.com/air-quality/v2/current-conditions?lat=" + lat + "&lon=" + lng+"&key="+ BREEZO_METER_API_KEY;
        JsonNode jsonNode = restTemplate.exchange(aqiApi, HttpMethod.GET, httpEntity, JsonNode.class).getBody();
        System.out.println(jsonNode);
        AirQualityData airQualityD = new AirQualityData();
        airQualityD.setTime(jsonNode.get("data").get("datetime").asText());
        airQualityD.setAqi(jsonNode.get("data").get("indexes").get("baqi").get("aqi").asInt());
        return airQualityD;
    }
    public PollenAdvice getPollenDataFromZip(String lat, String lng){
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        HttpEntity httpEntity = new HttpEntity(headers);
        String pollenApi = "https://api.breezometer.com/pollen/v2/forecast/daily?lat=" + lat + "&lon=" + lng+"&key="+ BREEZO_METER_API_KEY + "&days=" + "1";
        JsonNode jsonNode = restTemplate.exchange(pollenApi, HttpMethod.GET, httpEntity, JsonNode.class).getBody();
        System.out.println(jsonNode);
        PollenAdvice pollenAdvice = new PollenAdvice();
        pollenAdvice.setTime(jsonNode.get("data").get("datetime").asText());
        pollenAdvice.setPollenGrass(jsonNode.get("data").get("types").get("grass").get("in_season").asBoolean());
        pollenAdvice.setPollenTree(jsonNode.get("data").get("types").get("tree").get("in_season").asBoolean());
        pollenAdvice.setPollenWeed(jsonNode.get("data").get("types").get("weed").get("in_season").asBoolean());
        return pollenAdvice;
    }

    public FireAdvice setFireMessage(String lat, String lng, float radius) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        HttpEntity httpEntity = new HttpEntity(headers);
        String fireApi = "https://api.breezometer.com/fires/v1/current-conditions?lat=" + lat + "&lon=" + lng + "&key=" + BREEZO_METER_API_KEY + "&radius=" + radius;
        JsonNode jsonNode = restTemplate.exchange(fireApi, HttpMethod.GET, httpEntity, JsonNode.class).getBody();
        System.out.println(jsonNode);
        FireAdvice fireD = new FireAdvice();
        fireD.setTime(jsonNode.get("data").get("datetime").asText());
        String returnMessage = "";
        fireD.setActivity(jsonNode.get("data").get("details").get("status").asText());
        fireD.setFireSize(jsonNode.get("data").get("details").get("fire_size").get("value").asText()+" "+jsonNode.get("data").get("details").get("fire_size").get("units").asText());
        return fireD;
    }

    public Location getLocation(int zipCode) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        HttpEntity httpEntity = new HttpEntity(headers);
        JsonNode jsonNode = restTemplate.exchange(zipCodeUrl.replace("{zipcode}", String.valueOf(zipCode)), HttpMethod.GET, httpEntity, JsonNode.class).getBody();
        Location location = new Location();
        System.out.println(jsonNode);
        location.setLat(String.valueOf(jsonNode.get(String.valueOf(zipCode)).get("lat").asDouble()));
        location.setLng(String.valueOf(jsonNode.get(String.valueOf(zipCode)).get("lng").asDouble()));
        return location;
    }

    public AdviceData advice(int zipCode) throws APIException {
        Location location = getLocation(zipCode);
        OWM owm = new OWM((API_KEY));
        CurrentWeather currentWeather = owm.currentWeatherByZipCode(zipCode);
        CurrentUVIndex currentUVIndex = owm.currentUVIndexByCoords(Double.valueOf(location.getLat()), Double.valueOf(location.getLng()));
        AirQualityData airQualityData = getAirQualityDataFromZip(location.getLat(), location.getLng());
        SunAdvice sunAdvice = new SunAdvice();
        HumidityAdvice humidityAdvice = new HumidityAdvice();
        WindAdvice windAdvice = new WindAdvice();
        RainAdvice rainAdvice = new RainAdvice();
        UVAdvice uvAdvice = new UVAdvice();
        SnowAdvice snowAdvice = new SnowAdvice();
        PollenAdvice pollenAdvice = new PollenAdvice();
        FireAdvice fireAdvice = new FireAdvice();
        AdviceData adviceData = new AdviceData();

        String city = currentWeather.getCityName();
        adviceData.setCity(city);

        Date sunrise = currentWeather.getSystemData().getSunriseDateTime();
        Date sunset = currentWeather.getSystemData().getSunsetDateTime();
        sunAdvice.setSunrise(sunrise);
        sunAdvice.setSunset(sunset);
        int temp = (int)( 1.8 * (currentWeather.getMainData().getTemp() - 273) + 32);
        int temp2 = (int)(currentWeather.getMainData().getTemp()-273);
        double uv = currentUVIndex.getValue();
        double windSpeed = currentWeather.getWindData().getSpeed();
        boolean hasRain = currentWeather.hasRainData();
        double humidityPercent = currentWeather.getMainData().getHumidity();
        int airQuality = airQualityData.getAqi();
        boolean hasSnow = currentWeather.hasSnowData();
        boolean pollenG = pollenAdvice.getPollenGrass();
        boolean pollenW = pollenAdvice.getPollenWeed();
        boolean pollenT = pollenAdvice.getPollenTree();
        String fireActivity = fireAdvice.getActivity();
        String fireSize = fireAdvice.getFireSize();

        sunAdvice.setTemperatureC(temp2);
        sunAdvice.setTemperatureF(temp);
        rainAdvice.setHasRain(hasRain);
        humidityAdvice.setHumidityPercent(humidityPercent);
        uvAdvice.setUVAIndexValue(uv);
        windAdvice.setSpeed(windSpeed);
        airQualityData.setAqi(airQuality);
        snowAdvice.setHasSnow(hasSnow);
        pollenAdvice.setPollenGrass(pollenG);
        pollenAdvice.setPollenTree(pollenT);
        pollenAdvice.setPollenWeed(pollenW);
        fireAdvice.setFireSize(fireSize);
        fireAdvice.setActivity(fireActivity);
        String message = "";

        if (temp >= 70){
            message = "Stay in the shade. Bring water, wear cool clothing, and take a pair of sunglasses.";
            sunAdvice.setMessage(message);
            sunAdvice.setIcon("Hot.png");
        }else if (temp>=60&&temp<70){
            message = "It's warm outside.";
            sunAdvice.setMessage(message);
            sunAdvice.setIcon("Warm.jpeg");
        }
        else if (temp<=60&&temp>40){
            message = "Bring a jacket.";
            sunAdvice.setMessage(message);
            sunAdvice.setIcon("LightJacket.jpg");
        }
        else if (temp<=20){
            message = "It's very cold. Reconsider going out. Hypothermia is a real risk, and ice may form.";
            sunAdvice.setMessage(message);
            sunAdvice.setIcon("Freezing.jpeg");
        }
        else{
            message="It's cold out. Layer up!";
            sunAdvice.setMessage(message);
            sunAdvice.setIcon("ThickJacket.jpeg");
        }
        if (currentWeather.getWindData().getSpeed() >= 40&& currentWeather.getWindData().hasSpeed()==true){
            message = "High winds (40mph+). Drive safe.";
            windAdvice.setMessage(message);
            windAdvice.setIcon("Windy.jpeg");
        }
        else if (currentWeather.getWindData().getSpeed() >= 15&& currentWeather.getWindData().hasSpeed()==true){
            message = "Somewhat windy. Sports might be hard.";
            windAdvice.setMessage(message);
            windAdvice.setIcon("Windy.jpeg");
        }
        else{
            message = "Wind speed is less than 15mph. Nothing to worry about.";
            windAdvice.setMessage(message);
            windAdvice.setIcon("NoWind.gif");
        }
        if (currentWeather.hasRainData()==true){
            message = "Take an umbrella!";
            rainAdvice.setMessage(message);
            rainAdvice.setIcon("Umbrella.jpeg");
        }
        else{
            message = "No rain.";
            rainAdvice.setMessage(message);
            rainAdvice.setIcon("NoRain.png");
        }
        if (currentUVIndex.getValue()>=10){
            message="Wear lots of sunscreen, cover all exposed parts. Wear protective clothing, sunglasses, and a hat.";
            uvAdvice.setMessage(message);
            uvAdvice.setIcon("SunscreenSunglasses.jpeg");
        }
        else if (currentUVIndex.getValue()<10&&currentUVIndex.getValue()>=6){
            message="Wear a healthy amount of sunscreen. Cover yourself.";
            uvAdvice.setMessage(message);
            uvAdvice.setIcon("SunscreenSPF50.jpeg");
        }
        else if (currentUVIndex.getValue()<6&&currentUVIndex.getValue()>=3){
            message="Wear a moderate amount of sunscreen.";
            uvAdvice.setMessage(message);
            uvAdvice.setIcon("SunscreenSPF30.jpg");
        }
        else if (currentUVIndex.getValue()<3&&currentUVIndex.getValue()>=0){
            message="Wear a little bit of sunscreen, but don't worry too much.";
            uvAdvice.setMessage(message);
            uvAdvice.setIcon("SunscreenSPF10.jpg");
        }
        if (humidityPercent>=75){
            message="It will feel very damp and your sweat won't cool you off, so wear cool clothing. Fog is likely.";
            humidityAdvice.setMessage(message);
            humidityAdvice.setIcon("Humid.jpg");
        }
        else if (humidityPercent<=75&&humidityPercent>=51){
            message = "It will feel moist and may feel warm outside.";
            humidityAdvice.setMessage(message);
            humidityAdvice.setIcon("MildlyHumid.png");
        }
        else if(humidityPercent==50){
            message ="Average humidity.";
            humidityAdvice.setMessage(message);
            humidityAdvice.setIcon("NormalHumidity.jpg");
        }
        else if (humidityPercent<50){
            message="Bring moisturizer and water. It is dry outside.";
            humidityAdvice.setMessage(message);
            humidityAdvice.setIcon("Dry.jpg");
        }
        else if (humidityPercent>75&&windSpeed>29&&currentWeather.hasRainData()==true){
            rainAdvice.setMessage("A storm may be forming! Stay inside.");
            rainAdvice.setIcon("Storm.jpg");
        }
        if (airQuality>=301){
            message = "Hazardous. Don't go outside.";
            airQualityData.setMessage(message);
            airQualityData.setIcon("Hazardous.png");
        }
        else if (airQuality<=300&&airQuality>=201){
            message = "Very Unhealthy. Stay in, or go with a face-mask.";
            airQualityData.setMessage(message);
            airQualityData.setIcon("VeryUnhealthy.jpg");
        }
        else if(airQuality<=200&&airQuality>=151){
            message = "Unhealthy. Facial covering recommended.";
            airQualityData.setMessage(message);
            airQualityData.setIcon("Unhealthy.jpg");
        }
        else if(airQuality<=150&&airQuality>=101){
            message = "Unhealthy for sensitive groups (e.g. Elderly, sick, asthmatic).";
            airQualityData.setMessage(message);
            airQualityData.setIcon("UnhealthyFor.jpg");
        }
        else if(airQuality<=100&&airQuality>=51){
            message = "Moderate.";
            airQualityData.setMessage(message);
            airQualityData.setIcon("Moderate.jpg");
        }
        else if(airQuality<=50&&airQuality>=0){
            message = "Healthy.";
            airQualityData.setMessage(message);
            airQualityData.setIcon("Healthy.jpg");
        }

        if (currentWeather.hasSnowData()==true){
            message = "It's snowing. Stay in or wear a snow jacket. Use tire chains";
            snowAdvice.setMessage(message);
            snowAdvice.setIcon("Snowing.png");
            snowAdvice.setAdvertisment("TireChains.png");
        }
        else if (currentWeather.hasSnowData()==false){
            message = "It's not snowing, but its good to be prepared.";
            snowAdvice.setMessage(message);
            snowAdvice.setIcon("NoSnow.jpeg");
            snowAdvice.setAdvertisment("TireChains.png");
        }
        if (pollenG == true){
            pollenAdvice.setGrassMessage("Stay indoors, or take allergy pills.");
            pollenAdvice.setIcon("extremePollen.jpg");
            pollenAdvice.setAdvertisment("Flonase.png");
        }
        else if (pollenG == false){
            pollenAdvice.setGrassMessage("There's no grass pollen to worry about.");
            pollenAdvice.setIcon("nothing.png");
            pollenAdvice.setAdvertisment("Flonase.png");
        }
        else {
            pollenAdvice.setGrassMessage("No data available.");
            pollenAdvice.setIcon("nothing.png");
        }
        if (pollenT == true){
            pollenAdvice.setTreeMessage("Stay indoors, or take allergy pills and precautions.");
            pollenAdvice.setIcon("extremePollen.jpg");
            pollenAdvice.setAdvertisment("Flonase.png");
        }
        else if (pollenT == false){
            pollenAdvice.setTreeMessage("There's no tree pollen to worry about.");
            pollenAdvice.setIcon("nothing.png");
            pollenAdvice.setAdvertisment("Flonase.png");
        }
        else {
            pollenAdvice.setTreeMessage("No data available.");
            pollenAdvice.setIcon("nothing.png");
        }
        if (pollenW == true){
            pollenAdvice.setWeedMessage("Stay indoors, or take allergy pills.");
            pollenAdvice.setIcon("extremePollen.jpg");
            pollenAdvice.setAdvertisment("Flonase.png");
        }
        else if (pollenW == false){
            pollenAdvice.setWeedMessage("There's no weed pollen to worry about.");
            pollenAdvice.setIcon("nothing.png");
            pollenAdvice.setAdvertisment("Flonase.png");
        }
        else {
            pollenAdvice.setWeedMessage("No data available.");
            pollenAdvice.setIcon("nothing.png");
        }
        try {
            if (fireAdvice.getActivity().equalsIgnoreCase("active")) {
                fireAdvice.setMessage("There's an active fire. Stay away, and use a breathing mask if needed. It is " + fireAdvice.getFireSize() + " big.");
                fireAdvice.setIcon("Extreme.jpeg");
            }
            else {
                fireAdvice.setMessage("No fires here!");
                fireAdvice.setIcon("nothing.png");
            }
        } catch (Exception E){
            fireAdvice.setActivity("No");
            fireAdvice.setFireSize("fire data");
            fireAdvice.setMessage("available");
            fireAdvice.setIcon("nothing.png");
        }

        adviceData.setHumidityAdvice(humidityAdvice);
        adviceData.setRainAdvice(rainAdvice);
        adviceData.setUvAdvice(uvAdvice);
        adviceData.setWindAdvice(windAdvice);
        adviceData.setSunAdvice(sunAdvice);
        adviceData.setSnowAdvice(snowAdvice);
        adviceData.setAirQualityData(airQualityData);
        adviceData.setPollenAdvice(pollenAdvice);
        adviceData.setFireAdvice(fireAdvice);
        return adviceData;
    }

    public static void main(String[] args) {
        WeatherService service = new WeatherService();

    }
}
