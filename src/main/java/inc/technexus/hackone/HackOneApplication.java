package inc.technexus.hackone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HackOneApplication {

	public static void main(String[] args) {
		SpringApplication.run(HackOneApplication.class, args);
	}

}
